using System;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static ScreenType currentScreen {  get; private set; }
    public GameObject mainScreen;
    public GameObject gameScreen;
    public GameObject settingsScreen;
    private static UIManager self; 
    void Awake()
    {
        self = this;
    }

    public static void SetScreen(ScreenType type)
    {
        DeactivateAll();
        switch (type)
        {
            case ScreenType.Main:
                self.mainScreen.SetActive(true);
                break;
            case ScreenType.Game:
                self.gameScreen.SetActive(true);
                break;
            case ScreenType.Settings:
                self.settingsScreen.SetActive(true);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        currentScreen = type;
    }

    public static void DeactivateAll()
    {
        self.mainScreen.SetActive(false);
        self.gameScreen.SetActive(false);
        self.settingsScreen.SetActive(false);
    }
    
}

public enum ScreenType
{
    Main, Game, Settings
}